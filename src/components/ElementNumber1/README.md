# ElementNumber1

<!-- STORY -->

## Introduction

ElementNumber1 is an easy-to-use component.

## Usage

```javascript
import { ElementNumber1 } from 'some-package-name';
```

## Example use

```javascript
const myPage = props => {
  return (
    <main>
      <ElementNumber1 />
    </main>
  );
};
```

## Properties

- `className` - is the class name of the component

| propName  | propType | defaultValue | isRequired |
| --------- | -------- | ------------ | ---------- |
| className | string   | -            | -          |
