import React from 'react';
import { shallow, render, mount } from 'enzyme';
import ElementNumber1 from './ElementNumber1';

describe('ElementNumber1', () => {
  let props;
  let shallowElementNumber1;
  let renderedElementNumber1;
  let mountedElementNumber1;

  const shallowTestComponent = () => {
    if (!shallowElementNumber1) {
      shallowElementNumber1 = shallow(<ElementNumber1 {...props} />);
    }
    return shallowElementNumber1;
  };

  const renderTestComponent = () => {
    if (!renderedElementNumber1) {
      renderedElementNumber1 = render(<ElementNumber1 {...props} />);
    }
    return renderedElementNumber1;
  };

  const mountTestComponent = () => {
    if (!mountedElementNumber1) {
      mountedElementNumber1 = mount(<ElementNumber1 {...props} />);
    }
    return mountedElementNumber1;
  };  

  beforeEach(() => {
    props = {};
    shallowElementNumber1 = undefined;
    renderedElementNumber1 = undefined;
    mountedElementNumber1 = undefined;
  });

  // Shallow / unit tests begin here
 
  // Render / mount / integration tests begin here
  
});
