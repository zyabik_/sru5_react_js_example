import React from 'react';
import PropTypes from 'prop-types';
import styles from './ElementNumber1.scss';

// const ElementNumber1 = props => (
	{/*<div>This is a component called ElementNumber1.</div>*/}
// );

// todo: Unless you need to use lifecycle methods or local state,
// write your component in functional form as above and delete
// this section. 
class ElementNumber1 extends React.Component {
  render() {
    return <div>This is a component called ElementNumber1.</div>;
  }
}

const ElementNumber1PropTypes = {
	// always use prop types!
};

ElementNumber1.propTypes = ElementNumber1PropTypes;

export default ElementNumber1;
