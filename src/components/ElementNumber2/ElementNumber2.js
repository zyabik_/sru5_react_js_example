import React from 'react';
import PropTypes from 'prop-types';
import styles from './ElementNumber2.scss';

// const ElementNumber2 = props => (
// 	<div>This is a component called ElementNumber2.</div>
// );

// todo: Unless you need to use lifecycle methods or local state,
// write your component in functional form as above and delete
// this section. 
class ElementNumber2 extends React.Component {
  render() {
    return <div>This is a component called ElementNumber2.</div>;
  }
}

const ElementNumber2PropTypes = {
	// always use prop types!
};

ElementNumber2.propTypes = ElementNumber2PropTypes;

export default ElementNumber2;
