import React from 'react';
import { shallow, render, mount } from 'enzyme';
import ElementNumber2 from './ElementNumber2';

describe('ElementNumber2', () => {
  let props;
  let shallowElementNumber2;
  let renderedElementNumber2;
  let mountedElementNumber2;

  const shallowTestComponent = () => {
    if (!shallowElementNumber2) {
      shallowElementNumber2 = shallow(<ElementNumber2 {...props} />);
    }
    return shallowElementNumber2;
  };

  const renderTestComponent = () => {
    if (!renderedElementNumber2) {
      renderedElementNumber2 = render(<ElementNumber2 {...props} />);
    }
    return renderedElementNumber2;
  };

  const mountTestComponent = () => {
    if (!mountedElementNumber2) {
      mountedElementNumber2 = mount(<ElementNumber2 {...props} />);
    }
    return mountedElementNumber2;
  };  

  beforeEach(() => {
    props = {};
    shallowElementNumber2 = undefined;
    renderedElementNumber2 = undefined;
    mountedElementNumber2 = undefined;
  });

  // Shallow / unit tests begin here
 
  // Render / mount / integration tests begin here
  
});
