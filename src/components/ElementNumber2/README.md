# ElementNumber2

<!-- STORY -->

## Introduction

ElementNumber2 is an easy-to-use component.

## Usage

```javascript
import { ElementNumber2 } from 'some-package-name';
```

## Example use

```javascript
const myPage = props => {
  return (
    <main>
      <ElementNumber2 />
    </main>
  );
};
```

## Properties

- `className` - is the class name of the component

| propName  | propType | defaultValue | isRequired |
| --------- | -------- | ------------ | ---------- |
| className | string   | -            | -          |
