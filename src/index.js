import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import ElementNumber1 from './components/ElementNumber1/ElementNumber1';
import ElementNumber2 from './components/ElementNumber2/ElementNumber2';

ReactDOM.render(<ElementNumber1 />, document.getElementById('elementNumber1'));
ReactDOM.render(<ElementNumber2 />, document.getElementById('elementNumber2'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

